//
//  Place.swift
//  PerfectPlaces
//
//  Created by Tobiasz Dobrowolski on 02.01.2018.
//  Copyright © 2018 Tobiasz Dobrowolski. All rights reserved.
//

import UIKit

class Place {
    
    var name: String
    var description: String
    var photo: UIImage?
    var localization: String
    
    init?(name: String, description: String, photo: UIImage?, localization: String) {
        
        if name.isEmpty { return nil }
        
        self.name = name
        self.description = description
        self.photo = photo
        self.localization = localization
        
    }
    
}
