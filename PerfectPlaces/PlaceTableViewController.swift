//
//  PlaceTableViewController.swift
//  PerfectPlaces
//
//  Created by Tobiasz Dobrowolski on 03.01.2018.
//  Copyright © 2018 Tobiasz Dobrowolski. All rights reserved.
//

import UIKit

class PlaceTableViewController: UITableViewController {
    
    var places = [Place]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 65, 0)
        
        let button = UIButton(frame: CGRect(origin: CGPoint (x: 0, y: self.view.frame.height - 65), size: CGSize(width: self.view.frame.width, height: 65)))
        button.setTitle("add new", for: .normal)
        
        let containerEffect = UIBlurEffect(style: .dark)
        let containerView = UIVisualEffectView(effect: containerEffect)
        containerView.frame = button.bounds
        
        containerView.isUserInteractionEnabled = false // Edit: so that subview simply passes the event through to the button
        
        button.insertSubview(containerView, belowSubview: button.titleLabel!)
        
        let vibrancy = UIVibrancyEffect(blurEffect: containerEffect)
        let vibrancyView = UIVisualEffectView(effect: vibrancy)
        vibrancyView.frame = containerView.bounds
        containerView.contentView.addSubview(vibrancyView)
        
        //vibrancyView.contentView.addSubview(button.titleLabel!)
        
        self.navigationController?.view.addSubview(button)
        
        loadSamplePlaces()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "PlaceTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PlaceTableViewCell else {
            
            fatalError("Not an instance of PlaceTableViewCell.")
            
        }

        let place = places[indexPath.row]
        
        cell.nameLabel.text = place.name
        cell.photoImageView.image = place.photo

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        navigationController?.pushViewController(destination, animated: true)
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    @IBAction func unwindToPlaceList(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.source as? PlaceViewController, let place = sourceViewController.place {
            
            let newIndexPath = IndexPath(row: places.count, section: 0)
            places.append(place)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func loadSamplePlaces() {
        
        let photo1 = UIImage(named: "1")
        let photo2 = UIImage(named: "2")
        let photo3 = UIImage(named: "3")
        let photo4 = UIImage(named: "4")
        let photo5 = UIImage(named: "5")
        
        guard let place1 = Place(name: "West Union", description: "Cool place", photo: photo1, localization: "xd") else {
            fatalError("Unable to instantiate place1")
        }
        guard let place2 = Place(name: "Sunny Corner", description: "Cool place", photo: photo2, localization: "xd") else {
            fatalError("Unable to instantiate place2")
        }
        guard let place3 = Place(name: "Westermint", description: "Cool place", photo: photo3, localization: "xd") else {
            fatalError("Unable to instantiate place3")
        }
        guard let place4 = Place(name: "Uijo", description: "Cool place", photo: photo4, localization: "xd") else {
            fatalError("Unable to instantiate place4")
        }
        guard let place5 = Place(name: "Grand Mountains", description: "Cool place", photo: photo5, localization: "xd") else {
            fatalError("Unable to instantiate place5")
        }
        
        places += [place1, place2, place3, place4, place5]

    }

}
